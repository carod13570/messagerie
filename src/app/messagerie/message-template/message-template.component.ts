import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-template',
  templateUrl: './message-template.component.html',
  styleUrls: ['./message-template.component.css']
})
export class MessageTemplateComponent implements OnInit {

  @Input() message: any;

  constructor() { }

  ngOnInit(): void {
  }

}
