import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { faCoffee, faEnvelope, faUser } from '@fortawesome/free-solid-svg-icons';
import { CustomBackendService } from '../service/custom-backend.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  count: number;
  faEnvelope = faEnvelope;
  faUser = faUser;

  constructor(private $backendMessage: CustomBackendService) { }

  ngOnInit(): void {
    //this.$backendMessage.getMessages((messages: Message[]) => this.count = messages.length);
  }

}
