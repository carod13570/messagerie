import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarMessagerieComponent } from './navbar-messagerie.component';

describe('NavbarMessagerieComponent', () => {
  let component: NavbarMessagerieComponent;
  let fixture: ComponentFixture<NavbarMessagerieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarMessagerieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarMessagerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
