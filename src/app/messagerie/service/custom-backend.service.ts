import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  BehaviorSubject, Observable } from 'rxjs';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class CustomBackendService {

  private results: any;
  private messages: Message[];
  private subject;

  constructor(private client: HttpClient) {
    this.subject = new BehaviorSubject({});
    this.synchroneMessages();
  }

    // getMessages (callback: any){
    //   this.client.get('http://localhost:6556/messages')
    //   .toPromise()
    //   .then((messages: any) => {
    //     callback(messages);
    //   })
    //   .catch((error) =>
    //     console.log(error)
    //   );
    // }

    getMessages() : Observable<Message[]>{
      return this.subject.asObservable();
    }

    synchroneMessages(){
      this.client.get('http://localhost:6556/messages')
      .toPromise()
      .then((message: Message[])=> this.subject.next(message))
      .catch(err => console.log(err))
    }





    postMessage (data: any){
      this.client.post('http://localhost:6556/createMessage', data).subscribe(result => {
        this.results = result;
        this.subject.next(this.results);
        console.log(result)
      })
    }

    refreshMessage(){
      return this.results;
    }
}
