import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message } from '../models/Message';
import { CustomBackendService } from '../service/custom-backend.service';

@Component({
  selector: 'app-display-messages',
  templateUrl: './display-messages.component.html',
  styleUrls: ['./display-messages.component.css']
})
export class DisplayMessagesComponent implements OnInit {

  messages: Message[];
  message: Message;
  id='';
  from='';
  subject='';
  body='';
  constructor(private $backendMessage: CustomBackendService) { }

  ngOnInit(): void {

  }

  createMessage(){

    const id = this.id;
    const from = this.from;
    const subject = this.subject;
    const body = this.body;

    const data = {
      id,
      from,
      subject,
      body
    }
    console.log(data);

    this.$backendMessage.postMessage(data);
  }

}
