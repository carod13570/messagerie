import { HttpClient } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, Input, OnInit } from '@angular/core';
import { CustomBackendService } from '../service/custom-backend.service';

@Component({
  selector: 'app-read-message',
  templateUrl: './read-message.component.html',
  styleUrls: ['./read-message.component.css']
})
export class ReadMessageComponent implements OnInit {

messages: any[];

@Input() maxMess = 10;

  constructor(private $backendMessage: CustomBackendService) { }

  ngOnInit(): void {
    //this.$backendMessage.getMessages((message: Message[]) => this.messages = message)
    this.$backendMessage.getMessages().subscribe(messages => this.messages = messages);
  }


}
