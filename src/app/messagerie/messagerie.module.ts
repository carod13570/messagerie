import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReadMessageComponent } from './read-message/read-message.component';
import { DisplayMessagesComponent } from './display-messages/display-messages.component';
import { NavbarMessagerieComponent } from './navbar-messagerie/navbar-messagerie.component';
import { HttpClientModule } from '@angular/common/http';
import { MessageTemplateComponent } from './message-template/message-template.component';
import { NotificationComponent } from './notification/notification.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ReadMessageComponent,
    DisplayMessagesComponent,
    NavbarMessagerieComponent,
    MessageTemplateComponent,
    NotificationComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    DisplayMessagesComponent,
    ReadMessageComponent,
    NavbarMessagerieComponent,
    NotificationComponent
  ]
})
export class MessagerieModule { }
