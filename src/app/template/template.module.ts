import { MessagerieModule } from './../messagerie/messagerie.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ContentComponent } from './content/content.component';



@NgModule({
  declarations: [
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    ContentComponent,

  ],
  imports: [
    CommonModule,
    MessagerieModule
  ],
  exports: [
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    ContentComponent
  ]
})
export class TemplateModule { }
